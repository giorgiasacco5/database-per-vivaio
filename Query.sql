# operazione 1 visualizza dati fornitore 
select *
from fornitori
where nomefornitore like "Mamma Mia";


# operazione 2 lista fornitori
select nomefornitore
from fornitori;

# operazione 3 modifica dati fornitori

update fornitori
set nomefornitore="Fioriture"
where iban like "FR45D0567345678901234567842";

# operazione 4 modifica un prodotto in negozio

update catalogopiante
set prezzopiante=3.7
where IDserialeP=1;

update catalogoattrezziarticoli
set prezzoAA=17.5
where IDserialeAA=1;

update pianteimportanti
set eta=2
where codicePI="BC6612BF";

update macchinari
set prezzomacchinari=3500
where codiceidentificativo="5112223W12";

#operzione visualizza dati prodotti 

select *
from catalogopiante
where specieP="tulipani";

#operazione visualizza
select * 
from clienti;

# visualizza ricavo dell'anno 2019 

create view ricavopiante(piante) as
           select sum(prezzopiante*numprodotto)
           from P_VS as p,CatalogoPiante as c,VociScontrino as v
           where c.IDserialeP=p.CatalogoPiante_IDserialeP and p.VociScontrino_Scontrino_codicescontrino=v.Scontrino_codicescontrino 
                and v.numerovoce=p.VociScontrino_numerovoce and Scontrino_codicescontrino in (select codicescontrino
                                                                                                 from Scontrino
                                                                                                 where datascontrino>20190101000000 and datascontrino<20191231235959);
      
 create view ricavoattrezziarticoli(attr) as     
			 select sum(prezzoAA*numprodotto)
             from AA_VS as a,catalogoAttrezziArticoli as c,VociScontrino as v
             where c.IDserialeAA=a.catalogoAttrezziArticoli_IDserialeAA and a.VociScontrino_Scontrino_codicescontrino=v.Scontrino_codicescontrino 
                  and v.numerovoce=a.VociScontrino_numerovoce and Scontrino_codicescontrino in (select codicescontrino
                                                                                                 from Scontrino
                                                                                                 where datascontrino>20190101000000 and datascontrino<20191231235959);
      
      
 create view ricavomacchinari(macc) as       
            select sum(prezzomacchinari*numprodotto)
            from M_VS as m, Macchinari as c,VociScontrino as v
			where m.Macchinari_codiceidentificativo=c.codiceidentificativo and m.VociScontrino_Scontrino_codicescontrino=v.Scontrino_codicescontrino 
                  and v.numerovoce=m.VociScontrino_numerovoce and Scontrino_codicescontrino in (select codicescontrino
                                                                                                 from Scontrino
                                                                                                 where datascontrino>20190101000000 and datascontrino<20191231235959);
                                                                                                 
create view ricavopianteimportanti(piaimp) as       
            select sum(prezzoPI*numprodotto)
			from PI_VS as p, PianteImportanti as i,VociScontrino as v
            where p.PianteImportanti_codicePI=i.codicePI and p.VociScontrino_Scontrino_codicescontrino=v.Scontrino_codicescontrino 
                  and v.numerovoce=p.VociScontrino_numerovoce and Scontrino_codicescontrino in (select codicescontrino
                                                                                                 from Scontrino
                                                                                                 where datascontrino>20190101000000 and datascontrino<20191231235959);
                  
create view listaricavi(ricavi) as
			select piante
			from ricavopiante
            
			union
            
            select attr
            from ricavoattrezziarticoli
            
            union 
            
            select macc
            from ricavomacchinari
            
            union
            
            select piaimp
            from ricavopianteimportanti;

select sum(ricavi)
from listaricavi;

# visualizza spesa dell'anno 2019

create view listaspesa(spesa) as 
            select sum(numerositaA*costoP)
            from AcquistoPiante  
            where dataP>'2019/01/01' and dataP<'2019/12/31'
            
            union
            
            select sum(numAAA*costoAA)
            from AcquistoAttrezziArticoli 
            where dataAA>'2019/01/01' and dataAA<'2019/12/31'
            
            union
            
            select sum(costoM)
            from Macchinari
            where dataM>'2019/01/01' and dataM<'2019/12/31'
            
            union
            
            select sum(costoPI)
            from A_PI as a , PianteImportanti as p
            where p.codicePI=a.PianteImportanti_codicePI and dataI>'2019/01/01' and dataI<'2019/12/31';

select sum(spesa)
from listaspesa;

# guadagno totale dell'anno 2019

create view spesa_e_ricavo(s_e_r) as
           select -sum(spesa)
           from listaspesa
           
           union
           
           select sum(ricavi)
           from listaricavi;

select sum(s_e_r)
from spesa_e_ricavo;

#visualizza acquisti di un cliente

select v.numprodotto as numerosita_acquisto, c.specieP as acquisto,c.prezzopiante as prezzo
from vociscontrino as v join catalogopiante as c join  p_vs
where Scontrino_codicescontrino=VociScontrino_Scontrino_codicescontrino and
      VociScontrino_numerovoce=numerovoce and
      CatalogoPiante_IDserialeP=IDserialeP and 
      Scontrino_codicescontrino in (select codicescontrino
								    from scontrino
                                    where Clienti_tessera=(select tessera
													from clienti
                                                    where nome="Mario" and
                                                    cognomeC="Rossi"))
union
select numprodotto as numerosita_acquisto, m.descrizione as acquisto, prezzomacchinari as prezzo
from vociscontrino as vv join macchinari as m join  m_vs as mv
where mv.VociScontrino_Scontrino_codicescontrino=Scontrino_codicescontrino and
      mv.VociScontrino_numerovoce=numerovoce and
      Macchinari_codiceidentificativo=codiceidentificativo and 
      Scontrino_codicescontrino in (select codicescontrino
								    from scontrino
                                    where Clienti_tessera=(select tessera
													from clienti
                                                    where nome="Mario" and
                                                    cognomeC="Rossi"))
union
select numprodotto as numerosita_acquisto, speciePI as acquisto, prezzoPI as prezzo
from vociscontrino join  pianteimportanti join  pi_vs 
where PianteImportanti_codicePI=Scontrino_codicescontrino and
      VociScontrino_numerovoce=numerovoce and
      PianteImportanti_codicePI=codicePI and 
	  Scontrino_codicescontrino in (select codicescontrino
								    from scontrino
                                    where Clienti_tessera=(select tessera
													from clienti
                                                    where nome="Mario" and
                                                    cognomeC="Rossi"))
union
select numprodotto as numerosita_acquisto, nomeAA as acquisto, prezzoAA as prezzo
from vociscontrino join  catalogoattrezziarticoli join  aa_vs
where VociScontrino_Scontrino_codicescontrino=Scontrino_codicescontrino and
      VociScontrino_numerovoce=numerovoce and
      catalogoAttrezziArticoli_IDserialeAA=IDserialeAA and 
	  Scontrino_codicescontrino in (select codicescontrino
								    from scontrino
                                    where Clienti_tessera=(select tessera
													from clienti
                                                    where nome="Mario" and
                                                    cognomeC="Rossi"));
                       
#operazione contare clienti ( inteso come numero di scontrini battuti) in dato periodo

select count(*) as numero_clienti
from scontrino 
where codicescontrino in (select codicescontrino
from clienti join vociscontrino join scontrino
where codicescontrino=Scontrino_codicescontrino and
      Clienti_tessera=tessera and
      datascontrino >= 20110101000000 and 
      datascontrino <=20200101000000);
      
#operazione visualizza dati clienti

select * 
from clienti
where nome="Mario" and cognomeC="Rossi";
                       
# visualizza ordine
select *
from Consegna
where codiceconsegna=1;

# modifica ordine (inserisco data di arrivo)
update Consegna
set dataarrivo='2020/05/06'
where codiceconsegna=3;

#registro lavoratori (vedo i lavoratori attualmente attivi)
select *
from Lavoratori
where datalicenziamento is null;

# visualizza dati lavoratore
select *
from Lavoratori
where tesserino=2;

#visualizza turni lavoratore
select Turni_inizio,Turni_fine
from L_T_V 
where Lavoratori_tesserino=1;

# modifica lavoratore (cambio nome)
update Lavoratori
set nomel='Mario'
where tesserino=1;

#visualizza consegne effuettuate da una ditta
select *
from Consegna
where dittaconsegna='DHL' and dataarrivo is not null;

#modifica dati vivaio (cambio nome)
update Vivaio
set nomeV='Asso Di Fiori'
where ID=1;
