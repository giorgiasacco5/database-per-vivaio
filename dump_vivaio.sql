-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema vivaio
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema vivaio
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vivaio` DEFAULT CHARACTER SET utf8 ;
USE `vivaio` ;

-- -----------------------------------------------------
-- Table `vivaio`.`Fornitori`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Fornitori` (
  `iban` CHAR(27) NOT NULL,
  `nomefornitore` VARCHAR(45) NOT NULL,
  `sedelegale` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iban`),
  UNIQUE INDEX `iban_UNIQUE` (`iban` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`PianteImportanti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`PianteImportanti` (
  `eta` SMALLINT(4) NULL,
  `codicePI` CHAR(8) NOT NULL,
  `descrizione` TEXT(250) NULL,
  `prezzoPI` DECIMAL(8,2) NOT NULL,
  `speciePI` VARCHAR(45) NOT NULL,
  `provenienza` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`codicePI`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`A_PI`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`A_PI` (
  `dataI` DATE NOT NULL,
  `costoPI` DECIMAL(8,2) NOT NULL,
  `Fornitori_iban` CHAR(27) NOT NULL,
  `PianteImportanti_codicePI` CHAR(8) NOT NULL,
  PRIMARY KEY (`Fornitori_iban`, `PianteImportanti_codicePI`),
  INDEX `fk_A-PI_PianteImportanti1_idx` (`PianteImportanti_codicePI` ASC) VISIBLE,
  CONSTRAINT `fk_A-PI_Fornitori1`
    FOREIGN KEY (`Fornitori_iban`)
    REFERENCES `vivaio`.`Fornitori` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_A-PI_PianteImportanti1`
    FOREIGN KEY (`PianteImportanti_codicePI`)
    REFERENCES `vivaio`.`PianteImportanti` (`codicePI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Vivaio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Vivaio` (
  `nomeV` VARCHAR(45) NOT NULL,
  `indirizzo` VARCHAR(45) NOT NULL,
  `mq` DECIMAL(10,2) NULL,
  `ID` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`PI_V`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`PI_V` (
  `PianteImportanti_codicePI` CHAR(8) NOT NULL,
  `Vivaio_ID` INT NOT NULL,
  PRIMARY KEY (`PianteImportanti_codicePI`, `Vivaio_ID`),
  INDEX `fk_PI_V_Vivaio1_idx` (`Vivaio_ID` ASC) VISIBLE,
  CONSTRAINT `fk_PI-V_PianteImportanti1`
    FOREIGN KEY (`PianteImportanti_codicePI`)
    REFERENCES `vivaio`.`PianteImportanti` (`codicePI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PI_V_Vivaio1`
    FOREIGN KEY (`Vivaio_ID`)
    REFERENCES `vivaio`.`Vivaio` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Macchinari`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Macchinari` (
  `codiceidentificativo` CHAR(10) NOT NULL,
  `prezzomacchinari` DECIMAL(8,2) NOT NULL,
  `descrizione` VARCHAR(45) NULL,
  `marca` VARCHAR(45) NOT NULL,
  `Fornitori_iban` CHAR(27) NOT NULL,
  `dataM` DATETIME NOT NULL,
  `costoM` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`codiceidentificativo`),
  UNIQUE INDEX `codice identificativo_UNIQUE` (`codiceidentificativo` ASC) VISIBLE,
  INDEX `fk_Macchinari_Fornitori1_idx` (`Fornitori_iban` ASC) VISIBLE,
  CONSTRAINT `fk_Macchinari_Fornitori1`
    FOREIGN KEY (`Fornitori_iban`)
    REFERENCES `vivaio`.`Fornitori` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`M_V`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`M_V` (
  `Macchinari_codiceidentificativo` CHAR(10) NOT NULL,
  `Vivaio_ID` INT NOT NULL,
  PRIMARY KEY (`Macchinari_codiceidentificativo`, `Vivaio_ID`),
  INDEX `fk_M_V_Vivaio1_idx` (`Vivaio_ID` ASC) VISIBLE,
  CONSTRAINT `fk_M-V_Macchinari1`
    FOREIGN KEY (`Macchinari_codiceidentificativo`)
    REFERENCES `vivaio`.`Macchinari` (`codiceidentificativo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_M_V_Vivaio1`
    FOREIGN KEY (`Vivaio_ID`)
    REFERENCES `vivaio`.`Vivaio` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`catalogoAttrezziArticoli`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`catalogoAttrezziArticoli` (
  `IDserialeAA` INT NOT NULL AUTO_INCREMENT,
  `nomeAA` VARCHAR(45) NOT NULL,
  `prezzoAA` DECIMAL(8,2) NOT NULL,
  `numAA` INT NOT NULL,
  PRIMARY KEY (`IDserialeAA`),
  UNIQUE INDEX `ID seriale AA_UNIQUE` (`IDserialeAA` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`AcquistoAttrezziArticoli`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`AcquistoAttrezziArticoli` (
  `codiceAA` CHAR(10) NOT NULL,
  `numAAA` INT NOT NULL,
  `costoAA` DECIMAL(8,2) NOT NULL,
  `dataAA` DATE NOT NULL,
  `Fornitori_iban` CHAR(27) NOT NULL,
  `catalogoAttrezziArticoli_IDserialeAA` INT NOT NULL,
  PRIMARY KEY (`codiceAA`),
  INDEX `fk_Acquisto attrezzi e articoli_Fornitori1_idx` (`Fornitori_iban` ASC) VISIBLE,
  INDEX `fk_AcquistoAttrezziArticoli_catalogoAttrezziArticoli1_idx` (`catalogoAttrezziArticoli_IDserialeAA` ASC) VISIBLE,
  CONSTRAINT `fk_Acquisto attrezzi e articoli_Fornitori1`
    FOREIGN KEY (`Fornitori_iban`)
    REFERENCES `vivaio`.`Fornitori` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AcquistoAttrezziArticoli_catalogoAttrezziArticoli1`
    FOREIGN KEY (`catalogoAttrezziArticoli_IDserialeAA`)
    REFERENCES `vivaio`.`catalogoAttrezziArticoli` (`IDserialeAA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`AcquistoPiante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`AcquistoPiante` (
  `numerositaA` INT NOT NULL,
  `dataP` DATE NOT NULL,
  `costoP` DECIMAL(8,2) NOT NULL,
  `codiceacquistoP` CHAR(10) NOT NULL,
  `Fornitori_iban` CHAR(27) NOT NULL,
  PRIMARY KEY (`codiceacquistoP`),
  INDEX `fk_Acquisto piante_Fornitori1_idx` (`Fornitori_iban` ASC) VISIBLE,
  CONSTRAINT `fk_Acquisto piante_Fornitori1`
    FOREIGN KEY (`Fornitori_iban`)
    REFERENCES `vivaio`.`Fornitori` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`CatalogoPiante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`CatalogoPiante` (
  `IDserialeP` INT NOT NULL AUTO_INCREMENT,
  `statocrescita` VARCHAR(45) NULL,
  `prezzopiante` DECIMAL(8,2) NOT NULL,
  `specieP` VARCHAR(45) NOT NULL,
  `numpiante` INT NOT NULL,
  PRIMARY KEY (`IDserialeP`),
  UNIQUE INDEX `ID seriale P_UNIQUE` (`IDserialeP` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`A_P`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`A_P` (
  `AcquistoPiante_codiceacquistoP` CHAR(10) NOT NULL,
  `CatalogoPiante_IDserialeP` INT NOT NULL,
  PRIMARY KEY (`AcquistoPiante_codiceacquistoP`, `CatalogoPiante_IDserialeP`),
  INDEX `fk_A-P_CatalogoPiante1_idx` (`CatalogoPiante_IDserialeP` ASC) VISIBLE,
  CONSTRAINT `fk_A-P_AcquistoPiante1`
    FOREIGN KEY (`AcquistoPiante_codiceacquistoP`)
    REFERENCES `vivaio`.`AcquistoPiante` (`codiceacquistoP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_A-P_CatalogoPiante1`
    FOREIGN KEY (`CatalogoPiante_IDserialeP`)
    REFERENCES `vivaio`.`CatalogoPiante` (`IDserialeP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`AA_V`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`AA_V` (
  `catalogoAttrezziArticoli_IDserialeAA` INT NOT NULL,
  `Vivaio_ID` INT NOT NULL,
  PRIMARY KEY (`catalogoAttrezziArticoli_IDserialeAA`, `Vivaio_ID`),
  INDEX `fk_AA_V_Vivaio1_idx` (`Vivaio_ID` ASC) VISIBLE,
  CONSTRAINT `fk_AA-V_catalogoAttrezziArticoli1`
    FOREIGN KEY (`catalogoAttrezziArticoli_IDserialeAA`)
    REFERENCES `vivaio`.`catalogoAttrezziArticoli` (`IDserialeAA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AA_V_Vivaio1`
    FOREIGN KEY (`Vivaio_ID`)
    REFERENCES `vivaio`.`Vivaio` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`P_V`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`P_V` (
  `CatalogoPiante_IDserialeP` INT NOT NULL,
  `Vivaio_ID` INT NOT NULL,
  PRIMARY KEY (`CatalogoPiante_IDserialeP`, `Vivaio_ID`),
  INDEX `fk_P_V_Vivaio1_idx` (`Vivaio_ID` ASC) VISIBLE,
  CONSTRAINT `fk_P-V_CatalogoPiante1`
    FOREIGN KEY (`CatalogoPiante_IDserialeP`)
    REFERENCES `vivaio`.`CatalogoPiante` (`IDserialeP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_P_V_Vivaio1`
    FOREIGN KEY (`Vivaio_ID`)
    REFERENCES `vivaio`.`Vivaio` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Clienti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Clienti` (
  `tessera` INT NOT NULL AUTO_INCREMENT,
  `cognomeC` VARCHAR(45) NULL,
  `nome` VARCHAR(45) NOT NULL,
  `ibanC` CHAR(27) NULL,
  `indirizzo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`tessera`),
  UNIQUE INDEX `tessera_UNIQUE` (`tessera` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Scontrino`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Scontrino` (
  `codicescontrino` INT NOT NULL AUTO_INCREMENT,
  `datascontrino` TIMESTAMP NOT NULL,
  `modalitapagamento` VARCHAR(45) NOT NULL,
  `Clienti_tessera` INT NOT NULL,
  PRIMARY KEY (`codicescontrino`),
  UNIQUE INDEX `codice scontrino_UNIQUE` (`codicescontrino` ASC) VISIBLE,
  INDEX `fk_Scontrino_Clienti1_idx` (`Clienti_tessera` ASC) VISIBLE,
  CONSTRAINT `fk_Scontrino_Clienti1`
    FOREIGN KEY (`Clienti_tessera`)
    REFERENCES `vivaio`.`Clienti` (`tessera`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`VociScontrino`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`VociScontrino` (
  `numprodotto` INT NOT NULL,
  `Scontrino_codicescontrino` INT NOT NULL,
  `numerovoce` INT NOT NULL,
  INDEX `fk_VociScontrino_Scontrino1_idx` (`Scontrino_codicescontrino` ASC) VISIBLE,
  PRIMARY KEY (`Scontrino_codicescontrino`, `numerovoce`),
  CONSTRAINT `fk_VociScontrino_Scontrino1`
    FOREIGN KEY (`Scontrino_codicescontrino`)
    REFERENCES `vivaio`.`Scontrino` (`codicescontrino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`M_VS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`M_VS` (
  `Macchinari_codiceidentificativo` CHAR(10) NOT NULL,
  `VociScontrino_Scontrino_codicescontrino` INT NOT NULL,
  `VociScontrino_numerovoce` INT NOT NULL,
  PRIMARY KEY (`Macchinari_codiceidentificativo`, `VociScontrino_Scontrino_codicescontrino`, `VociScontrino_numerovoce`),
  INDEX `fk_M-VS_VociScontrino1_idx` (`VociScontrino_Scontrino_codicescontrino` ASC, `VociScontrino_numerovoce` ASC) VISIBLE,
  CONSTRAINT `fk_M-VS_Macchinari1`
    FOREIGN KEY (`Macchinari_codiceidentificativo`)
    REFERENCES `vivaio`.`Macchinari` (`codiceidentificativo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_M-VS_VociScontrino1`
    FOREIGN KEY (`VociScontrino_Scontrino_codicescontrino` , `VociScontrino_numerovoce`)
    REFERENCES `vivaio`.`VociScontrino` (`Scontrino_codicescontrino` , `numerovoce`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`PI_VS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`PI_VS` (
  `PianteImportanti_codicePI` CHAR(8) NOT NULL,
  `VociScontrino_Scontrino_codicescontrino` INT NOT NULL,
  `VociScontrino_numerovoce` INT NOT NULL,
  PRIMARY KEY (`PianteImportanti_codicePI`, `VociScontrino_Scontrino_codicescontrino`, `VociScontrino_numerovoce`),
  INDEX `fk_PI-VS_VociScontrino1_idx` (`VociScontrino_Scontrino_codicescontrino` ASC, `VociScontrino_numerovoce` ASC) VISIBLE,
  CONSTRAINT `fk_PI-VS_PianteImportanti1`
    FOREIGN KEY (`PianteImportanti_codicePI`)
    REFERENCES `vivaio`.`PianteImportanti` (`codicePI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PI-VS_VociScontrino1`
    FOREIGN KEY (`VociScontrino_Scontrino_codicescontrino` , `VociScontrino_numerovoce`)
    REFERENCES `vivaio`.`VociScontrino` (`Scontrino_codicescontrino` , `numerovoce`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`AA_VS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`AA_VS` (
  `catalogoAttrezziArticoli_IDserialeAA` INT NOT NULL,
  `VociScontrino_Scontrino_codicescontrino` INT NOT NULL,
  `VociScontrino_numerovoce` INT NOT NULL,
  PRIMARY KEY (`catalogoAttrezziArticoli_IDserialeAA`, `VociScontrino_Scontrino_codicescontrino`, `VociScontrino_numerovoce`),
  INDEX `fk_AA-VS_VociScontrino1_idx` (`VociScontrino_Scontrino_codicescontrino` ASC, `VociScontrino_numerovoce` ASC) VISIBLE,
  CONSTRAINT `fk_AA-VS_catalogoAttrezziArticoli1`
    FOREIGN KEY (`catalogoAttrezziArticoli_IDserialeAA`)
    REFERENCES `vivaio`.`catalogoAttrezziArticoli` (`IDserialeAA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AA-VS_VociScontrino1`
    FOREIGN KEY (`VociScontrino_Scontrino_codicescontrino` , `VociScontrino_numerovoce`)
    REFERENCES `vivaio`.`VociScontrino` (`Scontrino_codicescontrino` , `numerovoce`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`P_VS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`P_VS` (
  `CatalogoPiante_IDserialeP` INT NOT NULL,
  `VociScontrino_Scontrino_codicescontrino` INT NOT NULL,
  `VociScontrino_numerovoce` INT NOT NULL,
  PRIMARY KEY (`CatalogoPiante_IDserialeP`, `VociScontrino_Scontrino_codicescontrino`, `VociScontrino_numerovoce`),
  INDEX `fk_P-VS_VociScontrino1_idx` (`VociScontrino_Scontrino_codicescontrino` ASC, `VociScontrino_numerovoce` ASC) VISIBLE,
  CONSTRAINT `fk_P-VS_CatalogoPiante1`
    FOREIGN KEY (`CatalogoPiante_IDserialeP`)
    REFERENCES `vivaio`.`CatalogoPiante` (`IDserialeP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_P-VS_VociScontrino1`
    FOREIGN KEY (`VociScontrino_Scontrino_codicescontrino` , `VociScontrino_numerovoce`)
    REFERENCES `vivaio`.`VociScontrino` (`Scontrino_codicescontrino` , `numerovoce`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Consegna`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Consegna` (
  `codiceconsegna` INT NOT NULL AUTO_INCREMENT,
  `dataarrivo` DATE NULL,
  `dittaconsegna` VARCHAR(45) NOT NULL,
  `VociScontrino_Scontrino_codicescontrino` INT NOT NULL,
  `VociScontrino_numerovoce` INT NOT NULL,
  PRIMARY KEY (`codiceconsegna`),
  UNIQUE INDEX `codice consegna_UNIQUE` (`codiceconsegna` ASC) VISIBLE,
  INDEX `fk_Consegna_VociScontrino1_idx` (`VociScontrino_Scontrino_codicescontrino` ASC, `VociScontrino_numerovoce` ASC) VISIBLE,
  CONSTRAINT `fk_Consegna_VociScontrino1`
    FOREIGN KEY (`VociScontrino_Scontrino_codicescontrino` , `VociScontrino_numerovoce`)
    REFERENCES `vivaio`.`VociScontrino` (`Scontrino_codicescontrino` , `numerovoce`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Lavoratori`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Lavoratori` (
  `tesserino` INT NOT NULL AUTO_INCREMENT,
  `nomeL` VARCHAR(45) NOT NULL,
  `cognomeL` VARCHAR(45) NOT NULL,
  `dataassunzione` DATE NOT NULL,
  `datalicenziamento` DATE NULL,
  PRIMARY KEY (`tesserino`),
  UNIQUE INDEX `tesserino_UNIQUE` (`tesserino` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Telefoni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Telefoni` (
  `numero` CHAR(10) NOT NULL,
  `Lavoratori_tesserino` INT NOT NULL,
  PRIMARY KEY (`numero`),
  UNIQUE INDEX `numero_UNIQUE` (`numero` ASC) VISIBLE,
  INDEX `fk_Telefoni_Lavoratori1_idx` (`Lavoratori_tesserino` ASC) VISIBLE,
  CONSTRAINT `fk_Telefoni_Lavoratori1`
    FOREIGN KEY (`Lavoratori_tesserino`)
    REFERENCES `vivaio`.`Lavoratori` (`tesserino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Mansioni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Mansioni` (
  `mansione` VARCHAR(45) NOT NULL,
  `Lavoratori_tesserino` INT NOT NULL,
  PRIMARY KEY (`mansione`, `Lavoratori_tesserino`),
  INDEX `fk_Mansioni_Lavoratori1_idx` (`Lavoratori_tesserino` ASC) VISIBLE,
  CONSTRAINT `fk_Mansioni_Lavoratori1`
    FOREIGN KEY (`Lavoratori_tesserino`)
    REFERENCES `vivaio`.`Lavoratori` (`tesserino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`Turni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`Turni` (
  `inizio` TIMESTAMP NOT NULL,
  `fine` TIMESTAMP NOT NULL,
  PRIMARY KEY (`inizio`, `fine`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `vivaio`.`L_T_V`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vivaio`.`L_T_V` (
  `Turni_inizio` TIMESTAMP NOT NULL,
  `Turni_fine` TIMESTAMP NOT NULL,
  `Lavoratori_tesserino` INT NOT NULL,
  `Vivaio_ID` INT NOT NULL,
  PRIMARY KEY (`Turni_inizio`, `Turni_fine`, `Lavoratori_tesserino`, `Vivaio_ID`),
  INDEX `fk_L-T-V_Turni1_idx` (`Turni_inizio` ASC, `Turni_fine` ASC) VISIBLE,
  INDEX `fk_L-T-V_Lavoratori1_idx` (`Lavoratori_tesserino` ASC) VISIBLE,
  INDEX `fk_L_T_V_Vivaio1_idx` (`Vivaio_ID` ASC) VISIBLE,
  CONSTRAINT `fk_L-T-V_Turni1`
    FOREIGN KEY (`Turni_inizio` , `Turni_fine`)
    REFERENCES `vivaio`.`Turni` (`inizio` , `fine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_L-T-V_Lavoratori1`
    FOREIGN KEY (`Lavoratori_tesserino`)
    REFERENCES `vivaio`.`Lavoratori` (`tesserino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_L_T_V_Vivaio1`
    FOREIGN KEY (`Vivaio_ID`)
    REFERENCES `vivaio`.`Vivaio` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
