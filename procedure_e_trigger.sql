#PROCEDURE

#creo procedura per inserire acquisto piante, aggiunta collegamento A_P e aggiornare numero piante in negozio

delimiter &&    
create procedure acquisto_piante(in numA int ,in data_P date ,in costo_P decimal(8,2),in codiceacquisto_P char(10),
in Forn_iban char(27),in IDser_P int)
begin
insert into AcquistoPiante (numerositaA,dataP,costoP,codiceacquistoP,Fornitori_iban)
    values (numA,data_P,costo_P,codiceacquisto_P,Forn_iban);
insert into A_P (AcquistoPiante_codiceacquistoP,CatalogoPiante_IDserialeP)
  values (codiceacquisto_P,IDser_P);
update CatalogoPiante
set  numpiante=numpiante+numA
where IDserialeP=IDser_P;
end ; &&
delimiter ;

#procedure per l'acquisto di piante importanti

delimiter &&    
create procedure acquisto_piante_import(in etaP smallint(4),in codice_PI char(8),in descr text(250),in px_PI decimal(8,2),
in specie varchar(45),in proven varchar(45),in dat_ac date, in costoPi decimal(8,2),in ibanF char(27))
begin
insert into pianteimportanti(eta,codicePI,descrizione,prezzoPI,speciePI,provenienza)
value(etaP,codice_PI,descr,px_PI,specie,proven);
insert into a_pi(dataI,costoPI,Fornitori_iban,PianteImportanti_codicePI)
value(dat_ac,costoPi,ibanF,codice_PI);
end ; &&
delimiter ;

#procedure per l'acquisto attrezzi e articoli

delimiter &&    
create procedure acquisto_att_e_art(in IDserAA int,in cod_AA char(10) ,in n_AAA int,in cost_AA decimal(8,2),
 in data_AA date,in F_iban char(27))
begin
insert into acquistoattrezziarticoli(codiceAA,numAAA,costoAA,dataAA,Fornitori_iban,catalogoAttrezziArticoli_IDserialeAA)
value (cod_AA,n_AAA,cost_AA,data_AA,F_iban,IDserAA);
update catalogoattrezziarticoli
set numAA=numAA+n_AAA
where IDserialeAA=IDserAA;
end ; &&
delimiter ;

# Procedure per la vendita macchinari.
#La vendita non viene permessa se il macchinario è gia stato venduto

delimiter &&    
create procedure vendita_macchinari(in numprodot int,in Scontrinocodicescontrino int,in numerovoc int,in Macchinari_cod char(10))
begin
if Macchinari_cod in (select Macchinari_codiceidentificativo
                      from M_VS) then 
SIGNAL SQLSTATE '75001' set message_text='il macchinario è già stato venduto';
end if;
if Macchinari_cod not in (select Macchinari_codiceidentificativo
                      from M_VS) then 
insert into VociScontrino(numprodotto,Scontrino_codicescontrino,numerovoce)
value (numprodot,Scontrinocodicescontrino,numerovoc);
insert into M_VS(Macchinari_codiceidentificativo,VociScontrino_Scontrino_codicescontrino,VociScontrino_numerovoce)
value (Macchinari_cod,Scontrinocodicescontrino,numerovoc);
end if;
end ; &&
delimiter ;

#procedure per la vendita di piante.
#La vendita non viene permessa se si cerca di vendere un numero di piante maggiore rispetto a quello disponibile

delimiter &&    
create procedure vendita_Piante(in numprodot int,in Scontrinocodicescontrino int,in numerovoc int,
                                in CatalogoPiante_ID int)
begin
if numprodot > (select numpiante
			    from catalogopiante
                where CatalogoPiante_ID=IDserialeP) then
SIGNAL SQLSTATE '75001' set message_text = 'Stai cercando di vendere piu piante di quante ce ne siano in negozio';
end if;
if numprodot <= (select numpiante
			    from catalogopiante
                where CatalogoPiante_ID=IDserialeP) then
insert into VociScontrino(numprodotto,Scontrino_codicescontrino,numerovoce)
value (numprodot,Scontrinocodicescontrino,numerovoc);
insert into P_VS(CatalogoPiante_IDserialeP,VociScontrino_Scontrino_codicescontrino,VociScontrino_numerovoce)
value (CatalogoPiante_ID,Scontrinocodicescontrino,numerovoc);
update CatalogoPiante
set numpiante=numpiante-numprodot
where IDserialeP=CatalogoPiante_ID;
end if;
end ; &&
delimiter ;

#procedure vendita piante importanti.
#La ventita non viene permessa se la pianta importante è già stata venduta

delimiter &&    
create procedure vendita_Pianteimportanti(in numprodot int,in Scontrinocodicescontrino int,in numerovoc int,
                                in PianteImportanti_cod char(8))
begin
if PianteImportanti_cod in (select PianteImportanti_codicePI
				                from pi_vs) then 
SIGNAL SQLSTATE '75001' set message_text = 'La pianta importante è già stata venduta';
end if;
if PianteImportanti_cod not in (select PianteImportanti_codicePI
				                from pi_vs) then 
insert into VociScontrino(numprodotto,Scontrino_codicescontrino,numerovoce)
value (numprodot,Scontrinocodicescontrino,numerovoc);
insert into PI_VS(PianteImportanti_codicePI,VociScontrino_Scontrino_codicescontrino,VociScontrino_numerovoce)
value (PianteImportanti_cod,Scontrinocodicescontrino,numerovoc); 
end if;
end ; &&
delimiter ;

#procedure per vendita attrezzi e articoli.
#La vendita non viene permessa se si cerca di vendere un numero di attrezzi e articoli maggiore rispetto a quello disponibile

delimiter &&    
create procedure vendita_attrezziarticoli(in numprodot int,in Scontrinocodicescontrino int,in numerovoc int,
                                in catalogoAttrezziArticoli_ID int)
begin
if numprodot > (select numAA
				from catalogoattrezziarticoli
                where IDserialeAA=catalogoAttrezziArticoli_ID) then
SIGNAL SQLSTATE '75001' set message_text = 'Sto provando a vendere piu prodotti di quelli disponibili in negozio';
end if;
if numprodot <= (select numAA
				from catalogoattrezziarticoli
                where IDserialeAA=catalogoAttrezziArticoli_ID) then
insert into VociScontrino(numprodotto,Scontrino_codicescontrino,numerovoce)
value (numprodot,Scontrinocodicescontrino,numerovoc);
insert into AA_VS(catalogoAttrezziArticoli_IDserialeAA,VociScontrino_Scontrino_codicescontrino,VociScontrino_numerovoce)
value (catalogoAttrezziArticoli_ID,Scontrinocodicescontrino,numerovoc);
update catalogoAttrezziArticoli
set numAA=numAA-numprodot
where IDserialeAA=catalogoAttrezziArticoli_ID;
end if;
end ; &&
delimiter ;

#procedure L T V

delimiter &&    
create procedure lav_turni_in_neg(in inizioturno timestamp,in fineturno timestamp,in tesserino int, in vivaio int )
begin
insert into l_t_v(Turni_inizio,Turni_fine,Lavoratori_tesserino,Vivaio_ID)
value (inizioturno,fineturno,tesserino,vivaio);
end; &&
delimiter ;

#TRIGGER

# La fine di un turno non può precedere l'inizio

 delimiter //
 CREATE TRIGGER check_turni BEFORE insert ON turni
       FOR EACH ROW
       BEGIN
           IF NEW.fine < new.inizio THEN
           SIGNAL SQLSTATE '75001' set message_text = 'L orario di fine turnodeve essere maggiore di quello di inizio turno';
		   set new.fine=null;
           end if;
       END;//
delimiter ;

# la data di consegna non deve essere successiva alla data di arrivo

 delimiter //
 CREATE TRIGGER check_upd_consegna BEFORE update ON consegna
       FOR EACH ROW
       BEGIN
           IF NEW.dataarrivo <(select datascontrino
                               from scontrino
                               where new.VociScontrino_Scontrino_codicescontrino=codicescontrino)THEN
           SIGNAL SQLSTATE '75001' set message_text = 'La consegna deve essere fatta dopo la data di acquisto';
		   set new.dataarrivo=null;
           end if;
       END;//
delimiter ;

 delimiter //
 CREATE TRIGGER check_ins_consegna BEFORE insert ON consegna
       FOR EACH ROW
       BEGIN
           IF NEW.dataarrivo <(select datascontrino
                               from scontrino
                               where new.VociScontrino_Scontrino_codicescontrino=codicescontrino)THEN
           SIGNAL SQLSTATE '75001' set message_text = 'La consegna deve essere fatta dopo la data di acquisto';
		   set new.dataarrivo=null;
           end if;
       END;//
delimiter ;

# la data di licenzimento non deve essere successiva alla data di assunzione

 delimiter //
 CREATE TRIGGER licenziamento BEFORE update ON lavoratori
       FOR EACH ROW
       BEGIN
           IF NEW.datalicenziamento <old.dataassunzione then
           SIGNAL SQLSTATE '75001' set message_text = 'Non si puo licenziare prima di assumere';
		   set new.datalicenziamento=null;
           end if;
       END;//
delimiter ;




