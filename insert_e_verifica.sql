#inserimento fornitori

insert into Fornitori (iban,nomefornitore,sedelegale)
  values ("IT44D0123456789012345678913","Mamma Mia","Italia"),
         ("IT45D0567345678901234567912","Viva I Fiori","Italia"),
		 ("FR45D0567345678901234567842","Forniture","Francia"),
         ("SP45D0567345678901345672642","Flower Poewr","Spagna"),
         ("IT44D0123456789012345678915","TreDiFiori","Italia");
         
  #inserisco piante in catalogo piante 
  
  insert into CatalogoPiante (IDserialeP,statocrescita,prezzopiante,specieP,numpiante)
	values (null,"medio",3.50,"tulipani",35),(null,"medio",3.20,"camelie",10),(null,"medio",3.10,"orchidee",5);  
    
# acquisto piante

call acquisto_piante(10,"2015/02/15",1.5,"1112223330","IT44D0123456789012345678913",2);
call acquisto_piante(14,"2015/03/15",1.5,"1111111111","IT44D0123456789012345678915",1);

    #inserisco macchinari

insert into macchinari(codiceidentificativo,prezzomacchinari,descrizione,marca,Fornitori_iban,dataM,costoM)
value("1212121212",4300.00,"trattore","GreenM","FR45D0567345678901234567842",'2019/10/2',3200),
("1233321212",4700.00,"escavatore","GreenM","FR45D0567345678901234567842",'2019/10/2',3300),
("5112223W12",3300.00,"Tosaerba Industriale","GreenM","IT44D0123456789012345678913",'2019/10/2',2500);

#inserisco piante importanti

insert into pianteimportanti(eta,codicePI,descrizione,prezzoPI,speciePI,provenienza)
value(80,"AC1212BF","ulivo secolare della riserva di Matera",5000,"Ulivo","Marera"),
(null,"BC6612BF","Rara pianta equatoriare",250,"Giglio D'acqua","Zimbdaue");

#acquisto piante impotanti

call acquisto_piante_import(1,"PIA9988C",null,3000,"Boabab","Madagascar",'2019/03/14',2000,"SP45D0567345678901345672642");

#inserisco vivaio

insert into Vivaio (nomeV,indirizzo,mq,ID)
        values ("4 Di Fiori"," via dei Gerani 32",300.75,null),	
               ("5 Di Fiori"," via dei Tulipani 11",150.33,null),
               ("6 Di Fiori"," via dei Gelsomini 176",432.00,null),
               ("8 Di Fiori"," via dei Girasoli 45",270.21,null);
               
#inserisco in catalogo attrezzi e articoli

insert into catalogoAttrezziArticoli(IDserialeAA,nomeAA,prezzoAA,numAA)
            values (null,"rastrello",18.50,33),
				   (null,"zappa",20.50,20),
				   (null,"vaso",5.50,60),
				   (null,"sottovaso",2.50,42);
                   
#acquisto attrezzi e articoli

call acquisto_att_e_art(2,"AAABBBCCC1",22,10.5,'2019/02/12',"IT44D0123456789012345678913");
call acquisto_att_e_art(2,"NNNDBBCCC1",222,1.5,'2019/03/12',"IT44D0123456789012345678913");


#aggiungo elementi nelle entità M_V PI_V AA_V P_V

insert into m_v(Macchinari_codiceidentificativo,Vivaio_ID)
values ("1212121212",1),("5112223W12",3),("1233321212",2);

insert into pi_v(PianteImportanti_codicePI,Vivaio_ID)
values("AC1212BF",1),("BC6612BF",1),("PIA9988C",3);

insert into aa_v(catalogoAttrezziArticoli_IDserialeAA,Vivaio_ID)
values (1,4),(2,2),(3,3),(4,1);

insert into p_v(CatalogoPiante_IDserialeP,Vivaio_ID)
value (1,1),(2,2),(3,4);


#inserimento clienti

insert into Clienti(tessera,cognomeC,nome,ibanC,indirizzo)
values (null,"Rossi","Mario","IT88D0123456789012745678915","via delle Rose 13"),
       (null,"Verdi","Luigi","IT88A9123456789012745678915","via delle Ortensie 11"),
	   (null,"Bianchi","Diego","IT88E8123456789012745678915","via delle Margherite 156");
       
 #inserimento scontrino
 
insert into Scontrino (codicescontrino,datascontrino,modalitapagamento,Clienti_tessera)
     values(null,20071215134555,"bancomat",1),
           (null,20121215134555,"contanti",2),
           (null,20191215134555,"bancomat",3);
           
# vendita macchinari
call vendita_macchinari(1,1,1,"1212121212");
call vendita_macchinari(1,2,2,"1233321212");

# vendita piante

call vendita_Piante(25,3,3,1);
call vendita_Piante(3,2,1,2);

#vendita piante importanti

call vendita_Pianteimportanti(1,3,1,"AC1212BF");
call vendita_Pianteimportanti(1,2,3,"BC6612BF");

#vendita attrezzi e articoli

call vendita_attrezziarticoli(20,1,2,1);
call vendita_attrezziarticoli(7,1,3,2);

#inseriamo dei turni 

insert into turni(inizio,fine)
value (20190314080000,20190314120000),(20190314120000,20190314160000),(20190314160000,20190314200000);

#inserimento lavoratori

insert into lavoratori(tesserino,nomeL,cognomeL,dataassunzione,datalicenziamento)
value (null,"Maria","Antonietta",'2018/09/13',null),(null,"Biaggio","Antonacci",'2008/07/13',null),
(null,"Luigi","Quattordicesimo",'2018/03/13',null),(null,"Margerita","Vicario",'2008/04/09','2009/02/12');

# L T V

call lav_turni_in_neg(20190314080000,20190314120000,1,1);
call lav_turni_in_neg(20190314120000,20190314160000,2,2);

#assegno mansioni

insert into mansioni (mansione,Lavoratori_tesserino)
value ("Vivaista",1),("Commesso",2),("Cassiere",2),("Magazziniere",3),("Commesso",4);

#assegno telefoni

insert into telefoni(numero,Lavoratori_tesserino)
value("3932722822",1),("3339993338",2),("0899198822",3),("3333339999",3),("0676788888",4);

#inserisco consegna

insert into consegna(codiceconsegna,dataarrivo,dittaconsegna,VociScontrino_Scontrino_codicescontrino,VociScontrino_numerovoce)
value(null,'2008/01/09',"FastDelivery",1,1),(null,'2019/12/20',"DHL",3,3),(null,null,"DHL",2,3);

#VERIFICA TRIGGER

# verifica trigger turni

insert into turni(inizio,fine)
value(20081210000000,20071210000000);

insert into turni(inizio,fine)
value(20071210000000,20081210000000);

#verifica trigger consegna

update consegna
set dataarrivo='2019/10/10'
where codiceconsegna=3;

update consegna
set dataarrivo='2000/10/10'
where codiceconsegna=3;

# verifica trigger lavoratori

update lavoratori
set datalicenziamento='2018/08/13'
where tesserino=1;

update lavoratori
set datalicenziamento='2020/01/13'
where tesserino=1;

###VERIFICA PROCEDURE

#controllo di non poter vendere più piante di quante ne ho disponibili

call vendita_Piante(600,3,8,1);

#controllo di non poter vendere più attrezzi e articoli di quanti ne ho disponibili

call vendita_attrezziarticoli(900,1,12,1);

#controllo di non poter vendere un macchinario già venduto

call vendita_macchinari(1,1,10,"1212121212");

#controllo di non poter vendere una piante importante già venduta

call vendita_Pianteimportanti(1,3,9,"AC1212BF");

